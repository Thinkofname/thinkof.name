---
layout: post
title:  "UniverCity - Released into early access"
comments: 18
---

The game is now available to buy in early access!

UniverCity is a university management game being programmed in the Rust
programming language.

<iframe src="https://store.steampowered.com/widget/808160/" frameborder="0" width="646" height="220"></iframe>

As stated above, the game is in early access and is not complete. There
will be changes and additions down the line and finishing it may take
some time. You may wish to wait until its further along before buying
it.

## Trello

To help track planned/wanted additions I've opened up a Trello. [It can
be found here with a preview below.][trello] Most of the things discussed
below should already be on the board.

<iframe src="https://trello.com/b/8d5NZopM.html" frameborder="0" width="750" height="400"></iframe>

## Plans

I've already had to fix some pretty bad bugs already just before launch
and I assume there will be more still. I plan to squash them as they
pop up.

I've been watching the early videos that have been popping up and taking
notes of what needs fixing. There are a few things I've seen that I aim
to fix first before working on new features.

* **Warn when a room doesn't have the required staff** -
    I noticed in some cases people hadn't noticed that their staff had quit
    or they didn't have enough to work all the rooms. This was made worse
    by the fact the required staff for a room isn't actually displayed
    anywhere.

    I'll most likely first the first part with a notification and the second
    part I'll have to play around with a few ideas e.g. putting it under
    the room description
* **Move the edit room option from right click** - Saw some doing this by
    mistake. Ideally I'd move it to a button on the toolbar that you would
    press and then select a room to edit.

    This may also help with the fact that people don't realize you can edit
    the building to place chairs/plants/etc which again isn't explained
    anywhere.
* **In game clock** - Not sure how I forgot this, shouldn't be too hard to
    add.
* **Label inspectors/VIPs** - Inspectors/VIPs will walk around sometimes
    and rate your lessons. They are not selectable so its pretty hard to
    know what they are actually doing (specially since their model is
    similar to the professor's).

If you have played the game on youtube/twitch/etc please post it on the steam
forums and/or [tag me on twitter][twitter] so I can watch through it. So far
its been the best way for me to get feedback!

## Discord

If you use discord I have a general server which I'm planning to use for
UniverCity related things as well [here][discord].

## Subreddit

I've opened a subreddit for the game as per someones suggestion. It's
mostly empty currently but hopefully that'll change once I get some
time to put some work into it. [Here][subreddit]

[subreddit]: https://www.reddit.com/r/Univercity/
[trello]: https://trello.com/b/8d5NZopM
[twitter]: https://twitter.com/thinkofdeath
[discord]: https://discord.gg/wppmAAH