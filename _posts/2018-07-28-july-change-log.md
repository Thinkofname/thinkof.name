---
layout: post
title:  "UniverCity - Change log"
comments: 17
---

I finally put up the store page ready for an early access release which
will be available in about 2 weeks. The game is still rough and missing
a lot of important parts but I'm hoping to start getting feedback now.

UniverCity is a university management game being programmed in the Rust
programming language.

## Store page

<iframe src="https://store.steampowered.com/widget/808160/" frameborder="0" width="646" height="220"></iframe>

If you are interested please wishlist the game. Please note the game will
be released in early access, it's not done and still needs a lot of work.
You may wish to wait and see how it turns out first if you are not sure
on early access games.

## Gameplay

### Students can idle in buildings

This had been intended since buildings where added with a comment left in
the script about adding it. This was blocked on detecting which tiles a
building owns and letting the building update when a new room is built
which is a special thing that only happens in buildings so it was left
out. This finally exists so now students can stand in open spaces.

### New "room": Running track

{% asset running-track.jpg alt="Students running around a player made track" class='cimage' %}

A new room/lesson was added called 'Running track'. As the name implies
this has students running around a track as made by the player. You can
have multiple tracks and one will be randomly selected for each student.

### New objects

{% asset corner-computer.jpg alt="A small computer desk with computer" align='right' %}
{% asset whiteboard.jpg alt="A whiteboard mounted to a wall" align='left' %}

To make the lecture rooms look slightly different from each other I've added
two new objects. A corner computer for the computer science lecture and
a whiteboard for the maths lecture. These were the last two lectures missing
objects.

I currently haven't hooked up any interactions for these objects yet so they are
just for decoration.

### New room: Cafe

{% asset cafe.jpg alt="A cafe with tables and students sitting at some of those tables" class='cimage' %}

A fancier version of the snack stop that was already in the game. Students
sit at tables and have food brought to them instead of fetching it themselves.

### Completed and cleaned up stats screen

The stats screen has been sitting unfinished for a while now and was something
I wanted to finish before releasing to early access. There are four different
graphs to choose from:

#### Money
Current amount of money owned by the player over time

{% asset stats-money.jpg alt="" %}

#### Income/Outcome
Income and outcome for each interval

{% asset stats-income.jpg alt="" %}

#### Students
The number of students over time

{% asset stats-students.jpg alt="" %}

#### Grades
Grades given out over time

{% asset stats-grades.jpg alt="" %}

## Internal

### Diagonal movement

Whilst the game is built around a grid, only having 4 directions of movement made
moving in some directions look weird. I've allowed diagonal movement which has
fixed this and made most things look a bit more natural too.

### Minor things

* The "English basics" room has been removed. It seemed like a weird one
  to have at a university.
* Fixed some crashes with loading/saving entities in a room being edited

## Twitch

I haven't been streaming my work on [twitch here][twitch] lately
but sometimes I will pop up and stream for a bit.
Feel free to stop by and watch if I'm streaming.

## Subreddit

I've opened a subreddit for the game as per someones suggestion. It's
mostly empty currently but hopefully that'll change once I get some
time to put some work into it. [Here][subreddit]

[twitch]: https://www.twitch.tv/thinkofname
[subreddit]: https://www.reddit.com/r/Univercity/