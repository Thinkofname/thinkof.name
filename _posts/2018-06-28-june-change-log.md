---
layout: post
title:  "UniverCity - Change log"
comments: 16
---

Originally I was planning to spend most of this month on adding new
rooms and objects to build but an major issue with the UI rendering
came up which ended up taking all my time.

UniverCity is a university management game being programmed in the Rust
programming language.

## Gameplay

### Colour blindness improvements

{% asset colour-example.jpg alt="Example colour selections with the default (first)" class='cimage' %}

{% asset colour-options.jpg alt="The new colour menu" align='right' %}

I had a tester who is colourblind point out that telling when a room was the right
size or blocked by an object was pretty hard due to the colours used. To
help with this I made two changes.

Firstly, I added a tooltip whilst drawing out a room/building to show you the
current size of your selection, this combined with the size requires already
shown at the side of the screen should help you see how far away you are from
the required size.

The second change/addition is that you can now change the colour of the
placement grid when the placement is valid or invalid. This should let you
select colours that work best for you.

### Warnings when students/staff members are unable to reach a room

{% asset staff-stuck.jpg alt="A notification informing the player someone is stuck" align='left' %}

Previously, if students were unable to reach a class room the script would
ignore this and treat them as in the lesson anyway. This led to some funny
issues where students would be sitting in the halls having lessons and in
some cases teachers teaching in the middle of nowhere.

Scripts now check to see if the staff member/student made it into the lesson
and if they didn't the player will be notified.

### Staff can be fired

Minor change that removes a staff member so you don't have to pay for those
you don't need anymore.


### Plants visually decay

{% asset plant-decay.jpg alt="Showing a few of the variants of plant decay" align='right' %}

Plants have always decayed over time so that janitors would water them, however
there was no visual cues for the player that this was happening. Using the
texture swapping shown in the last blog post plants now will slowly die until
they are watered.

The transition between states isn't smooth (there are only 3 states currently)
but it gets the point across for now, hopefully in the future I can improve this.

### New room: Animal science lab

{% asset sloth.jpg alt="A sloth on a branch" align='left' %}

I was hoping to have more than one room added this month but I ran out of time
due to other issues. The animal science lab is part of the science lesson group.
I aim to have animals on display as part of the lesson, currently I only have
a sloth (by request) with a simple idle animation but I plan to have more.

This did allow me to test multiple models for a single object for the first time
(the static base and the animated sloth) which seem to work well enough.

## Internal

### Replaced the UI rendering

For a long time now I've been using [webrender][webrender] to render my user
interface via [stylish][stylish]. Webrender helped a lot in getting my
user interface looking nicer with shadows and better font rendering and
saved me a lot of time implementing this myself. However this hasn't been
without issue: rendering issues on some platform/devices ended up occuring
every now and again which is to be expected and was normally fixed whenever
I updated webrender however updating webrender has been a challenge. Webrender
isn't documented currently and breaks the API a lot which is expected as it
isn't `1.0` yet, updating normally involves me guessing from parameter names
and reading through commits trying to work out where things have moved too.

The latest issue I had made me change my mind on using webrender. Back in May
I failed a build review due to a crash in webrender and quickly realized
I didn't understand the webrender codebase well enough to be able to track
down issues like this so I [quickly hacked around it][hacky-fix] which was
built on top of an existing hacky fix. (I have been meaning to report this
but I haven't had the time to be able to work out if its my fault or not and
reduce the code down). I realized that if the game was released and an issue
popped up I'd be left unable to fix it.

About 2 weeks ago a tester reported an issue where the game would lock up
after dragging an object around for a bit. I tested this and quickly noticed
the memory usage had shot up to 2gb and rising. My "fix" for the crash had
caused webrender to not unload fonts and reload them every frame, attempting
to modify the "fix" to remove fonts first caused webrender to crash again.
So I decided it was time to rewrite the rendering without webrender, the
performance would not be great but at least I would understand it. It took
a little under two weeks to get the interface looking mostly like it did
before (I removed things like filters as I was unable to implement them myself).

I'd like to revisit using webrender for rendering the user interface for
a game in the future but for now I'm focused on getting ready for release.
On the bright side if I ever want to try and port to Vulkan this should
be easier now.

### Static link LuaJIT

This mainly helps me during development because the version of luajit on
my system didn't have any debug flags set hiding some issues with my
wrapper. I was already bundling the library on linux/windows so this
doesn't change too much.

### Minor things

* Zooming in now has a limit to prevent clipping into things
* Fixed students doing musical chairs when a new student registered
* `lua::Ref` now uses weak references to the lua state to prevent leaking it
* Fix toilet doors not closing, this broke at some point
* Fixed animations being frozen when loading a save. When animations weren't loaded
  the animation time was being `%=` with 0.0 producing `NaN` causing everything to
  break until it was reset. I wish it was a panic instead of a silent `NaN`.

## Twitch

I haven't been streaming my work on [twitch here][twitch] lately
but sometimes I will pop up and stream for a bit.
Feel free to stop by and watch if I'm streaming.

## Subreddit

I've opened a subreddit for the game as per someones suggestion. It's
mostly empty currently but hopefully that'll change once I get some
time to put some work into it. [Here][subreddit]

[twitch]: https://www.twitch.tv/thinkofname
[subreddit]: https://www.reddit.com/r/Univercity/
[steam]: http://store.steampowered.com/
[webrender]: https://github.com/servo/webrender
[stylish]: https://github.com/Thinkofname/stylish
[hacky-fix]: https://github.com/Thinkofname/stylish/commit/8f7d0f11fd25a0e9c67844aada9367807444fe18