{
    pkgs ? import <nixpkgs> {}
}:
with pkgs;
let
    jekyll_think = bundlerEnv rec {
        name = "thinkof.name-${version}";

        version = "0.0.1";
        inherit ruby;
        # expects Gemfile, Gemfile.lock and gemset.nix in the same directory
        gemdir = ./.;
    };
    source = builtins.filterSource
        (path: type: baseNameOf path != ".git"
            && baseNameOf path != ".jekyll-cache"
            && baseNameOf path != ".asset-cache"
            && baseNameOf path != "result"
            && baseNameOf path != ".vs"
        )
        ./.;
in runCommand "build-site" {
    buildInputs = [ jekyll_think ];
} ''
export LOCALE_ARCHIVE="${glibcLocales}/lib/locale/locale-archive"
export LANG=C.UTF-8
export LANGUAGE=C.UTF-8
export LC_ALL=C.UTF-8

mkdir $out
cp -r ${source}/* .
JEKYLL_ENV=production ${jekyll_think}/bin/bundle exec jekyll build -d $out
''