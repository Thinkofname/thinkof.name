---
layout: post
title:  "Privacy Policy"
---

No personal information is stored by this site, all web access logs have been
anonymized and do not contain IP addresses or user agent information.